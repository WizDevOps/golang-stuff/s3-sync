/*
MIT License

Copyright © 2019 Daniel-Andrei Minca

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
package cmd

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/dminca/bucketPolicyizer"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"regexp"
)

var syncCmd = &cobra.Command{
	Use:   "sync",
	Short: "Synchronize data between S3 Buckets",
	Long: `Copy data between S3 Buckets that reside in separate
AWS S3 Accounts. Example call:

s3-sync sync`,
	Run: func(cmd *cobra.Command, args []string) {
		srcBucket, destBucket, outDir := viper.GetString("src.bucket"), viper.GetString("dest.bucket"), viper.GetString("dest.dataOutputDir")
		srcAwsSession := initAWSSession(viper.GetString("src.account_number"), viper.GetString("src.account_role"))
		destAwsSession := initAWSSession(viper.GetString("dest.account_number"), viper.GetString("dest.account_role"))
		filesToCopy := getMatchingDirs(srcBucket, srcAwsSession)

		if viper.GetBool("dry_run") {
			logrus.Warn("Running in dry-run mode. No changes are done, these files match your RegEx pattern")
			for i := range filesToCopy {
				logrus.Info(filesToCopy[i])
			}
		} else {
			createBucketPolicy(srcBucket, viper.GetString("src.account_number"), viper.GetString("src.account_role"), "s3:GetObject", srcAwsSession)
			createBucketPolicy(destBucket, viper.GetString("src.account_number"), viper.GetString("src.account_role"), "s3:PutObject", destAwsSession)
			defer deleteBucketPolicy(srcBucket, srcAwsSession)
			defer deleteBucketPolicy(destBucket, destAwsSession)

			syncObjectsOperation(srcBucket, destBucket, outDir, filesToCopy, srcAwsSession)
		}

		// TODO: configure verbosity flags if you wanna see Debug
	},
}

// Copy objects from source to destination Bucket
// between AWS Accounts
//
// Takes in source, destination bucket and a slice of
// files to proceed to the copy operation
func syncObjectsOperation(srcBucket, destBucket, destDir string, files []string, s *session.Session) {
	svc := s3.New(s)

	for i := range files {
		_, err := svc.CopyObject(&s3.CopyObjectInput{
			Bucket:     aws.String(destBucket),
			CopySource: aws.String(srcBucket + "/" + files[i]),
			Key:        aws.String(destDir + "/" + files[i]),
		})
		if err != nil {
			logrus.Fatalf("unable to copy item from bucket %q to bucket %q, %v", srcBucket, destBucket, err)
		}
		err = svc.WaitUntilObjectExists(&s3.HeadObjectInput{Bucket: aws.String(destBucket), Key: aws.String(destDir + "/" + files[i])})
		if err != nil {
			logrus.Fatal("error while waiting for items to be copied, %v", err)
		}
	}
	logrus.Info("Copy job finished")
}

// Delete attached Bucket Policy
//
// Takes in a bucket and a dereferenced session.Session, outputs just
// and empty string if operation is successful
//
// To keep this script as secure as possible, revoke all permissions
// after our job is done, that way we are assured that we didn't
// let any security holes open
func deleteBucketPolicy(bucket string, s *session.Session) *s3.DeleteBucketPolicyOutput {
	svc := s3.New(s)
	input := &s3.DeleteBucketPolicyInput{
		Bucket: aws.String(bucket),
	}
	result, err := svc.DeleteBucketPolicy(input)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			default:
				logrus.Info(aerr.Error())
			}
		} else {
			// Print the error, cast err to awserr.Error to get the Code and
			// Message from an error.
			logrus.Error(err.Error())
		}
	}
	logrus.Info(result)
	return result
}

// Attach Bucket Policy
// 
// Takes in bucket, account, role verb and a dereferenced session.Session; outputs
// and empty string if successful
// 
// In order to sync data between S3 Buckets that reside in different
// AWS Accounts, one must attach a Bucket Policy that grants:
//    Role -> s3:ListBucket & s3:GetObject on the sender side
//    to sourceBucket/*
// 
//    Role -> s3:ListBucket & s3:PutObject on the receiver side
//    to destinationBucket/*
func createBucketPolicy(bucket, account, role, verb string, s *session.Session) *s3.PutBucketPolicyOutput {
	svc := s3.New(s)
	policy := bucketPolicyizer.EmptyPolicy()
	policyStatement := bucketPolicyizer.Statement{
		Sid:    "DelegateS3AccessForMigration",
		Effect: "Allow",
		Principal: bucketPolicyizer.Principal{
			AWS: []string{fmt.Sprintf("arn:aws:iam::%s:role/%s", account, role)},
		},
		Action: bucketPolicyizer.Action{
			"s3:ListBucket",
			verb,
		},
		Resource: bucketPolicyizer.Resource{
			fmt.Sprintf("arn:aws:s3:::%s", bucket),
			fmt.Sprintf("arn:aws:s3:::%s/*", bucket),
		},
	}
	policy.Statement = append(policy.Statement, policyStatement)
	policyJSON, _ := bucketPolicyizer.CompilePolicy(policy)
	input := &s3.PutBucketPolicyInput{
		Bucket: aws.String(bucket),
		Policy: aws.String(policyJSON),
	}
	result, err := svc.PutBucketPolicy(input)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			default:
				logrus.Info(aerr.Error())
			}
		} else {
			// Print the error, cast err to awserr.Error to get the Code and
			// Message from an error.
			logrus.Info(err.Error())
		}
	}
	logrus.Info(result)
	return result
}

// RegEx match a pattern and return the matching results
//
// Takes in a bucket and a dereferenced session.Session; outputs
// a slice of strings which are the matching results
//
// Compiles a POSIX RegEx pattern and tries to match that on a dataset
func getMatchingDirs(bucket string, s *session.Session) []string {
	ds := fetchDataSets(bucket, s)
	var matchingResults []string
	for i := range ds {
		pattern := regexp.MustCompilePOSIX(viper.GetString("regex"))
		if pattern.MatchString(*ds[i]) {
			matchingResults = append(matchingResults, *ds[i])
		}
	}
	logrus.Debug(matchingResults)
	return matchingResults
}

// Get list of datasets
//
// Takes in an AWS Session and returns a slice of dereferenced
// strings in the format
//
// 	<dir>/<dir>/file
// 	mydir/mysubdir/myfile.png
func fetchDataSets(bucket string, s *session.Session) []*string {
	var fullPathToFiles []*string
	contents := listBucketContents(bucket, s)
	for i := range contents.Contents {
		fullPathToFiles = append(fullPathToFiles, contents.Contents[i].Key)
		logrus.Debug(contents.Contents[i].Key)
	}
	return fullPathToFiles
}

// Get a list of files from S3 Bucket
//
// Traverses the full Bucket path directly to the file, meaning that if you
// have a file that resides in s3://bucketid/a/b/c/d/e/f/g/file.avro it will
// return the full path to the file (including the file itself)
//
// Maximum amount that can be returned is 1000 objects from the bucket.
// Consider tweaking the pagination if more objects
// are to be returned: (*S3) ListObjectsPages
func listBucketContents(bucket string, awsSession *session.Session) *s3.ListObjectsV2Output {
	svc := s3.New(awsSession)
	input := &s3.ListObjectsV2Input{
		Bucket:  aws.String(bucket),
		MaxKeys: aws.Int64(100),
	}
	result, err := svc.ListObjectsV2(input)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case s3.ErrCodeNoSuchBucket:
				logrus.Error(s3.ErrCodeNoSuchBucket, aerr.Error())
			default:
				logrus.Error(aerr.Error())
			}
		} else {
			logrus.Error(err.Error())
		}
	}
	logrus.Debug(result)
	return result
}

// Get list of Buckets available in a particular account
//
// Takes an AWS Session and returns a map[string]string of
// S3 Buckets
func fetchBuckets(awsSess *session.Session) []*s3.Bucket {
	svc := s3.New(awsSess)
	result, err := svc.ListBuckets(&s3.ListBucketsInput{})
	if err != nil {
		logrus.Fatalf("failed to list buckets: %v", err)
	}
	for _, bucket := range result.Buckets {
		logrus.Debugf("%v : %v\n", aws.StringValue(bucket.Name), bucket.CreationDate)
	}
	return result.Buckets
}

// Initialise an AWS Session
//
// This is a prerequisite in order to use any AWS API endpoints/resources
func initAWSSession(account, role string) *session.Session {
	sess, err := session.NewSessionWithOptions(
		session.Options{
			Profile: fmt.Sprintf("arn:aws:iam::%v:role/%v", account, role),
			Config: aws.Config{
				Region: aws.String("eu-central-1"),
			},
			SharedConfigState: session.SharedConfigEnable,
		},
	)
	if err != nil {
		logrus.Fatalf("Failed to auth: %v", err)
	}
	logrus.Debug(sess)
	return sess
}

func init() {
	rootCmd.AddCommand(syncCmd)
}

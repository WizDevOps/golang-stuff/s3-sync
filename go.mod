module gitlab.com/WizDevOps/golang-stuff/s3-sync

go 1.12

require (
	github.com/aws/aws-sdk-go v1.23.0
	github.com/dminca/bucketPolicyizer v0.0.0-20180616003603-31f2dacafd01
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.4.0
)

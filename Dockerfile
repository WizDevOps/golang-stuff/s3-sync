FROM golang:alpine as builder
ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux
ADD . /app
WORKDIR /app
RUN set -x \
    && apk add --update --no-cache git \
    && adduser -SDHh /app appuser \
    && mkdir -v /app/.aws \
    && go mod tidy \
    && go build -a \
        -installsuffix cgo \
        -ldflags '-extldflags "-static"' \
        -o /usr/bin/s3-sync .
USER appuser
